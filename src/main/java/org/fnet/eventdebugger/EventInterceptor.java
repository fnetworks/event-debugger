package org.fnet.eventdebugger;

import java.lang.reflect.Method;
import java.util.EventObject;

/**
 * Interface for all classes which intercept events.
 * Used by the {@link EventDebugger} to perform actions on intercepted events.
 *
 * @see DefaultEventInterceptor
 */
public interface EventInterceptor {

	/**
	 * Called when an event is intercepted.
	 *
	 * @param origin The original callback method.
	 * @param event  the {@link EventObject} that has been passed to the method.
	 */
	void onEventHandlerCalled(Method origin, EventObject event);
}
