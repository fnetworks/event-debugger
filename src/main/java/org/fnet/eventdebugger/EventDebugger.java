package org.fnet.eventdebugger;

import net.bytebuddy.ByteBuddy;
import net.bytebuddy.implementation.MethodDelegation;

import java.util.EventListener;

import static net.bytebuddy.matcher.ElementMatchers.isAbstract;
import static net.bytebuddy.matcher.ElementMatchers.takesArguments;

public final class EventDebugger {

	private EventDebugger() {
	}

	/**
	 * Creates a debug {@link EventListener} instance for the provided listener class.
	 * This method uses a default {@link EventInterceptor} implementation
	 *
	 * @param cls The {@link EventListener} class to create an instance of
	 * @param <T> The listener type. Must be a subclass of {@link EventListener}
	 * @return The newly created {@link EventListener} subclass
	 * @see DefaultEventInterceptor
	 */
	public static <T extends EventListener> T createDebugEventListener(Class<T> cls) {
		return createDebugEventListener(cls, new DefaultEventInterceptor());
	}

	/**
	 * Creates a debug {@link EventListener} instance for the provided listener class.
	 *
	 * @param cls         The {@link EventListener} class to create an instance of
	 * @param interceptor The {@link EventInterceptor} to use
	 * @param <T>         The listener type. Must be a subclass of {@link EventListener}
	 * @return The newly created {@link EventListener} subclass
	 */
	public static <T extends EventListener> T createDebugEventListener(Class<T> cls, EventInterceptor interceptor) {
		try {
			return new ByteBuddy()
					.subclass(cls)
					.method(isAbstract().and(takesArguments(1)))
					.intercept(MethodDelegation.to(new InterceptorTarget(interceptor)))
					.make()
					.load(EventDebugger.class.getClassLoader())
					.getLoaded()
					.newInstance();
		} catch (ReflectiveOperationException ignored) {
			return null;
		}
	}

}
