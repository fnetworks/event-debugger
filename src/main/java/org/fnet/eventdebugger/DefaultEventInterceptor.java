package org.fnet.eventdebugger;

import java.lang.reflect.Method;
import java.util.EventObject;

/**
 * Default implementation for {@link EventInterceptor}.<br>
 * Prints the content to {@link System#out} in the following format:
 * {@code className.methodName(): eventAsString}
 */
public class DefaultEventInterceptor implements EventInterceptor {
	@Override
	public void onEventHandlerCalled(Method origin, EventObject event) {
		System.out.printf("%s.%s(): %s%n", origin.getDeclaringClass().getSimpleName(), origin.getName(), event.toString());
	}
}
