package org.fnet.eventdebugger;

import net.bytebuddy.implementation.bind.annotation.Argument;
import net.bytebuddy.implementation.bind.annotation.Origin;

import java.lang.reflect.Method;
import java.util.EventObject;

/**
 * Internal
 */
public final class InterceptorTarget {
	private EventInterceptor interceptor;

	InterceptorTarget(EventInterceptor interceptor) {
		this.interceptor = interceptor;
	}

	@SuppressWarnings("unused")
	public void intercept(@Origin Method origin, @Argument(0) EventObject o) {
		interceptor.onEventHandlerCalled(origin, o);
	}

}
