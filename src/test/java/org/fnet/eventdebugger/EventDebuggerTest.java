package org.fnet.eventdebugger;

import org.junit.AfterClass;
import org.junit.Test;

import java.io.File;
import java.lang.reflect.Method;
import java.util.EventListener;
import java.util.EventObject;
import java.util.Objects;

import static org.junit.Assert.*;

public class EventDebuggerTest {

	public interface TestEventListener extends EventListener {
		void onEvent(EventObject o);
	}

	private static class TestEventObject extends EventObject {
		static final TestEventObject INSTANCE = new TestEventObject(new Object());

		private TestEventObject(Object source) {
			super(source);
		}
	}

	private static class ResultHolder {
		boolean success = false;
		Method origin = null;
		EventObject event = null;

		void success(Method origin, EventObject event) {
			this.success = true;
			this.origin = origin;
			this.event = event;
		}
	}

	@Test
	public void createDebugEventListener() throws NoSuchMethodException {
		ResultHolder result = new ResultHolder();
		TestEventListener listener = EventDebugger.createDebugEventListener(TestEventListener.class, result::success);
		assertNotNull(listener);
		listener.onEvent(TestEventObject.INSTANCE);
		assertTrue(result.success);
		assertSame(TestEventObject.INSTANCE, result.event);
		assertEquals(TestEventListener.class.getMethod("onEvent", EventObject.class), result.origin);
	}

	@AfterClass
	public static void tearDown() {
		for (File f : Objects.requireNonNull(new File(".").listFiles(f -> f.getName().startsWith(".attach_pid")))) {
			if (!f.delete())
				System.err.println("WARNING: File could not be deleted: " + f);
		}
	}

}