# Swing Event Debugger

Utility library for debugging swing events.
Captures all events and prints them to console (Configurable).

## Usage

This example will print all received events to the console:

```java
JFrame frame = new JFrame();
frame.addWindowListener(EventDebugger.createEventListener(WindowListener.class));
```

You can supply a custom EventInterceptor as second parameter to ``createEventListener``.